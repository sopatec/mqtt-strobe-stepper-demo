CFLAGS = -I paho.mqtt.c/src/ -L paho.mqtt.c/src
LDLIBS = -l readline -l paho-mqtt3c

all: main

paho.mqtt.c/src/libpaho-mqtt3c.so:
	cd paho.mqtt.c && cmake . && make

main: main.c | paho.mqtt.c/src/libpaho-mqtt3c.so
	$(CC) -o $@ $^ $(CFLAGS) $(LDLIBS)
