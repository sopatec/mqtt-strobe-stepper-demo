#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <signal.h>
#include <readline/readline.h>
#include <readline/history.h>

#include <termios.h>
#include <fcntl.h>

#include "MQTTClient.h"

/* Possible improvements:
 *  - instead of setting timeouts, waiting for all input to have been received,
 *    use serial START/STOP signals (if they exist)
 *  - create a function sendcmd(str/fmtstr,..) that wraps input and output since
 *    always one line is sent and one is received. This simplifies cleanup of
 *    the input buffer and allows echo for send and recv (debug)
 */

#define MQTT_ADDRESS        "tcp://localhost:1883"
#define MQTT_CLIENTID_PRE   "cleverprobe-" 
#define MQTT_STEPPER_TOPIC  "strobe-stepper-demo/STEPPER"
#define MQTT_STROBE_TOPIC   "strobe-stepper-demo/STROBE"
#define MQTT_PAYLOAD        "Hello World!"
#define MQTT_QOS            1
#define MQTT_TIMEOUT        10000L

#define ARRSIZE(arr) (sizeof(arr)/sizeof(arr[0]))
#define RETPRINT(...) {printf(__VA_ARGS__); return;}
#define RETPRINTV(val, ...) {printf(__VA_ARGS__); return val;}

enum device {
	NONE    = 0b000,
	STROBE  = 0b001,
	STEPPER = 0b010,
	ANYDEV  = 0b011, /* device mask */
	ANY     = 0b100
};

struct command {
	const char* name;
	void (*callback)();
	enum device target;
};

static void die(const char *errstr, ...);
static void update_msg(const char *errstr, ...);
static int init_serial(const char *device, struct termios *options);
static void sighandler(int sig);
static char* alloc_fmtstr(const char *fmtstr, ...);
static int fscanf_line(FILE *f, const char *fmtstr, ...);
static ssize_t sgetline(char **line, size_t *size, FILE *f);
static void fdflush(int fd, int usec);
static int await_input(int fd, int usec);
static int strpfcmp(const char *prefix, const char *str);
static int int_arg(int *val);

static const char* strobe_id_str();
static void strobe_id_set();
static void strobe_init();
static void strobe_power();
static void strobe_intensity();
static void strobe_status();

static void stepper_init();
static void stepper_step();
static int stepper_encoder_val();
static void stepper_encoder();
static int stepper_target_val();
static void stepper_target();
static void stepper_reset_pos();
static void stepper_await_halt();
static void stepper_posmode();
static void stepper_status();
static void stepper_step_mode();

static void list_commands();
static void info_msg();

static void serial_msg();

static void mqtt_connection_lost(void *ctx, char *cause);
static int mqtt_receive(void *ctx, char *name, int len, MQTTClient_message *msg);
static void mqtt_receive_ack(void *ctx, MQTTClient_deliveryToken dt);

struct command commands[] = {
	/* strobe commands */
	{"strobe_init", strobe_init, ANY},
	{"strobe_id", strobe_id_set, STROBE},
	{"strobe_power", strobe_power, STROBE},
	{"strobe_intensity", strobe_intensity, STROBE},
	{"strobe_status", strobe_status, STROBE},
	/* stepper commands */
	{"stepper_init", stepper_init, ANY},
	{"stepper_step", stepper_step, STEPPER},
	{"stepper_pos", stepper_target, STEPPER},
	{"stepper_enc", stepper_encoder, STEPPER},
	{"stepper_reset_pos", stepper_reset_pos, STEPPER},
	{"stepper_status", stepper_status, STEPPER},
	{"stepper_step_mode", stepper_step_mode, STEPPER},
	{"stepper_posmode", stepper_posmode, STEPPER},
	/* testing */
	{"list", list_commands, ANY},
	{"info", info_msg, ANY},
	/* misc */
	{"sendmsg", serial_msg, ANYDEV}
};

static char *args[20], *errormsg = NULL, strbuf[256];
static int errormsg_alloc = 0, serial_fd = -1, arg_count = 0, cmd_done = 1;
static FILE* serial_stream = NULL;
static enum device current_device = NONE;

/* strobe vars */
static int strobe_id = 1;
static const int STROBE_RECV_TIMEOUT = 100000;

/* stepper vars */
static int stepper_id = 1;
static const int STEPPER_RECV_TIMEOUT = 300000;

void
die(const char* errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	vprintf(errstr, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

void
update_msg(const char* errstr, ...)
{
	va_list ap;
	int size;

	va_start(ap, errstr);
	size = vsnprintf(NULL, 0, errstr, ap);
	va_end(ap);

	if (size < 0) return;

	if (errormsg) {
		if (size + 1 > errormsg_alloc) {
			errormsg = realloc(errormsg, size + 1);
			errormsg_alloc = size + 1;
		}
	} else {
		errormsg = malloc(size + 1);
		errormsg_alloc = size + 1;
	}

	va_start(ap, errstr);
	vsnprintf(errormsg, errormsg_alloc, errstr, ap);
	va_end(ap);
}

int
init_serial(const char *device, struct termios *options)
{
	/* close previous connection */
	if (serial_fd != -1) {
		close(serial_fd);
		serial_fd = -1;
	}

	if (serial_stream) {
		fclose(serial_stream);
		serial_stream = NULL;
	}

	/* read/write, no tty takeover and automatic sync of writes */
	serial_fd = open(device, O_RDWR | O_NOCTTY | O_SYNC | O_NONBLOCK);
	if (serial_fd == -1) {
		update_msg("failed to open serial device, permissions?");
		return EXIT_FAILURE;
	}

	if (tcsetattr(serial_fd, TCSANOW, options)) {
		update_msg("tcgetattr: unable to get device attributes (%s)",
				strerror(errno));
		close(serial_fd);
		return EXIT_FAILURE;
	}

	serial_stream = fdopen(serial_fd, "r");
	if (!serial_stream || setvbuf(serial_stream, NULL, _IONBF, 0)) {
		update_msg("fdopen: failed to open stream from fd (%s)",
				strerror(errno));
		close(serial_fd);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

void
sighandler(int sig)
{
	if (!cmd_done)
		exit(EXIT_FAILURE);
	rl_reset_line_state();
	rl_cleanup_after_signal();
	rl_replace_line("", 0);
	rl_crlf();
	rl_redisplay();
}

char*
alloc_fmtstr(const char* fmtstr, ...)
{
	va_list ap;
	size_t size;
	char* str;

	va_start(ap, fmtstr);
	size = vsnprintf(NULL, 0, fmtstr, ap);
	va_end(ap);
	if (size < 0) die("alloc_fmtstr: invalid format string\n");

	size += 1;
	str = malloc(size);
	if (!str) die("alloc_fmtstr: out of memory\n");

	va_start(ap, fmtstr);
	vsnprintf(str, size, fmtstr, ap);
	va_end(ap);

	return str;
}

int
fscanf_line(FILE* f, const char* scan_str, ...)
{
	char *line = NULL;
	va_list ap;
	size_t size = 0, n = 0;

	sgetline(&line, &size, f);
	if (line && size) {
		va_start(ap, scan_str);
		n = vsscanf(line, scan_str, ap);
		va_end(ap);
	}

	free(line);
	return n;
}

ssize_t
sgetline(char **line, size_t *size, FILE *f)
{
	ssize_t n;

	n = getline(line, size, f);
	if (n == -1)
		clearerr(f); /* clear EOF error */

	return n;
}

void
fdflush(int fd, int usec)
{
	char c;

	/* tcflush(fd, TCIFLUSH) does not clear data for read().. */
	if (await_input(fd, usec) > 0)
		while (read(fd, &c, 1) > 0);
}

int
await_input(int fd, int usec)
{
	int rc;
	fd_set fds;
	struct timeval tv;

	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	tv.tv_usec = usec;
	tv.tv_sec = 0;

	rc = select(fd + 1, &fds, NULL, NULL, &tv);
	if (rc == -1)
		return 0;

	if (rc && FD_ISSET(fd, &fds)) {
		usleep(usec); /* wait for rest of data */
		return 1;
	} else {
		return 0;
	}
}

int
strpfcmp(const char *prefix, const char *str)
{
	return strncmp(prefix, str, strlen(prefix));
}

int
int_arg(int *argval)
{
	int val;
	char *end;

	if (arg_count <= 1)
		return EXIT_FAILURE;

	val = strtol(args[1], &end, 10);
	if (end == args[1])
		RETPRINTV(EXIT_FAILURE, "Invalid value.\n");

	*argval = val;
	return EXIT_SUCCESS;
}

const char*
strobe_id_str()
{
	static char id[2] = { 0 };

	id[0] = strobe_id == -1 ? '\0' : '0' + strobe_id;
	return id;
}

void
strobe_id_set()
{
	int id;

	if (int_arg(&id))
		RETPRINT("Please supply an integer id.\n");

	strobe_id = id;
}

void
strobe_init()
{
	struct termios options = {
		.c_iflag = 0,
		.c_oflag = 0,
		.c_cflag = CREAD | CLOCAL | CS8 | B9600, /* read, no hw ints, 8bit */
		.c_lflag = 0,
		.c_cc[VTIME] = 10, /* read has 1 second timeout */
		.c_cc[VMIN] = 0	   /* timeout regardless of chars read */
	};

	if (arg_count <= 1)
		RETPRINT("Supply the path of a tty device\n");

	if (init_serial(args[1], &options) != EXIT_SUCCESS)
		RETPRINT("Initialization failed (%s)\n", errormsg);

	current_device = STROBE;
}

void
strobe_power()
{
	if (arg_count <= 1)
		RETPRINT("Supply a power value (on/off)\n");

	if (!strcmp(args[1], "off"))
		dprintf(serial_fd, "%sstroff\n", strobe_id_str());
	else if (!strcmp(args[1], "on"))
		dprintf(serial_fd, "%sstron\n", strobe_id_str());
	else
		printf("Invalid arg\n");
}

void
strobe_intensity()
{
	if (arg_count <= 1) {
		char *scan_str;

		if (strobe_id == -1)
			RETPRINT("Please set the probe id first\n");

		fdflush(serial_fd, STROBE_RECV_TIMEOUT);
		dprintf(serial_fd, "%stelint\n", strobe_id_str());

		scan_str = alloc_fmtstr("%stelint:%%5s", strobe_id_str());
		if (!await_input(serial_fd, STROBE_RECV_TIMEOUT)
				|| fscanf_line(serial_stream, scan_str, strbuf) != 1)
			printf("Communication failure\n");
		else
			printf("Current intensity: %s\n", strbuf);

		free(scan_str);
	} else {
		int val;

		val = atoi(args[1]);
		dprintf(serial_fd, "%ssetint%i\n", strobe_id_str(), val);
	}
}

void
strobe_status()
{
	char *scan_str;
	int i, val;

	if (strobe_id == -1)
		RETPRINT("Specify the probe id beforehand\n");

	fdflush(serial_fd, STROBE_RECV_TIMEOUT);
	dprintf(serial_fd, "%stelsta\n", strobe_id_str());

	scan_str = alloc_fmtstr("%stelsta:%%14s", strobe_id_str());
	if (!await_input(serial_fd, STROBE_RECV_TIMEOUT)
			|| fscanf_line(serial_stream, scan_str, strbuf) != 1) {
		printf("Communication failure.\n");
		goto cleanup;
	}

	printf("Returned status: %s\n", strbuf);

	for (i = 0; i < 14; i++) {
		val = strbuf[i] - '0';
		switch (i + 'a') {
		case 'a':
			printf("%-16s: ", "strobe");
			if (val == 2)
				printf("off\n");
			else if (val == 1)
				printf("off, keys are locked\n");
			else
				printf("on\n");
			break;
		case 'b':
			printf("%-16s: ", "intensityref");
			if (val == 1)
				printf("external\n");
			else
				printf("internal\n");
			break;
		case 'd':
			printf("%-16s: ", "burst");
			if (val == 1)
				printf("disabled\n");
			else
				printf("enabled\n");
			break;
		case 'e':
			printf("%-16s: ", "trigger signal");
			if (val == 3)
				printf("internal\n");
			else if (val == 2)
				printf("external (connector)\n");
			else if (val == 1)
				printf("external (video)\n");
			else
				printf("internal\n");
			break;
		case 'f':
		case 'g':
		case 'i':
		case 'j':
		case 'c':
		case 'k':
		case 'm':
			break; /* menu settings or not implemented, skip */
		case 'l':
			printf("%-16s: ", "device");
			if (val == 1)
				printf("unlocked\n");
			else
				printf("locked\n");
		}
	}

cleanup:
	free(scan_str);
}

void
stepper_init()
{
	struct termios options = {
		.c_iflag = ICRNL, /* pre processing, replace \r with \n */
		.c_oflag = OPOST | ONLCR, /* post processing, replace \n with \r */
		.c_cflag = CREAD | CRTSCTS | CS8 | B115200, /* read, hw flowctrl, 8b */
		.c_lflag = 0,
		.c_cc[VTIME] = 0, /* doesnt matter -- using O_NONBLOCK */
		.c_cc[VMIN] = 0   /* timeout regardless of chars read */
	};
	int flags;

	if (arg_count <= 1)
		RETPRINT("Supply the path of a tty device\n");

	if (init_serial(args[1], &options) != EXIT_SUCCESS)
		RETPRINT("Initialization failed (%s)\n", errormsg);

	current_device = STEPPER;
}

int
stepper_encoder_val()
{
	int val = -1;
	char *scan_str;

	fdflush(serial_fd, STEPPER_RECV_TIMEOUT);
	dprintf(serial_fd, "#%iI\n", stepper_id);

	scan_str = alloc_fmtstr("%iI%%i", stepper_id);
	if (!await_input(serial_fd, STEPPER_RECV_TIMEOUT)
			|| fscanf_line(serial_stream, scan_str, &val) != 1)
		printf("Communication failure.\n");

	free(scan_str);
	return val;
}

int
stepper_target_val()
{
	int val = -1;
	char *scan_str;

	fdflush(serial_fd, STEPPER_RECV_TIMEOUT);
	dprintf(serial_fd, "#%iC\n", stepper_id);

	scan_str = alloc_fmtstr("%iC%%i", stepper_id);
	if (!await_input(serial_fd, STEPPER_RECV_TIMEOUT)
			|| fscanf_line(serial_stream, scan_str, &val) != 1)
		printf("Communication failure\n.");

	free(scan_str);
	return val;
}

void
stepper_encoder()
{
	printf("%i\n", stepper_encoder_val());
}

void
stepper_target()
{
	printf("%i\n", stepper_target_val());
}

void
stepper_reset_pos()
{
	int val;
	char *end, *line = NULL, *scan_str;
	size_t size;

	if (int_arg(&val))
		val = 0;

	fdflush(serial_fd, STEPPER_RECV_TIMEOUT);
	dprintf(serial_fd, "#%iD%i\n", stepper_id, val);
	scan_str = alloc_fmtstr("%iD%i\n", stepper_id, val);
	if (await_input(serial_fd, STEPPER_RECV_TIMEOUT)
			&& sgetline(&line, &size, serial_stream) > 0
			&& !strcmp(scan_str, line))
		printf("Value set.\n");
	else
		printf("Communication failure\n.");

	free(line);
}

void
stepper_await_halt()
{
	int last = -1, cur = 0;

	while (last == -1 || last != (cur = stepper_encoder_val())) {
		last = cur;
		usleep(100000);
	}
}

void
stepper_posmode()
{
	int val;
	char *end, *line = NULL, *scan_str;
	size_t size;

	if (int_arg(&val))
		RETPRINT("Supply a position mode\n");

	dprintf(serial_fd, "#%ip%i\n", stepper_id, val);
	scan_str = alloc_fmtstr("%ip%i\n", stepper_id, val);
	if (await_input(serial_fd, STEPPER_RECV_TIMEOUT)
			&& sgetline(&line, &size, serial_stream) > 0
			&& !strcmp(scan_str, line))
		printf("Value set.\n");
	else
		printf("Communication failure\n.");

	free(line);
}

void
stepper_step()
{
	int val, step, vel;
	char *scan_str, *end = NULL;

	if (int_arg(&val))
		RETPRINT("Supply a target value\n");

	step = stepper_encoder_val();
	printf("Prev at pos: %i\n", step);

	dprintf(serial_fd, "#%is%i\n", stepper_id, val);
	dprintf(serial_fd, "#%iA\n", stepper_id);
	stepper_await_halt();
	dprintf(serial_fd, "#%iS\n", stepper_id);

	step = stepper_encoder_val();
	printf("Now at pos: %i\n", step);
}

void
stepper_status()
{
	int tmp, val;
	size_t n = 0;
	u_int64_t bit;
	char *line = NULL;

	fdflush(serial_fd, STEPPER_RECV_TIMEOUT);
	dprintf(serial_fd, "#%i$\n", stepper_id);

	/* id prefix is padded for some reason (non-standard)..
	 * read it as an int just to be sure */
	if (!await_input(serial_fd, 100000)
			|| fscanf_line(serial_stream, "%i$%i", &tmp, &val) != 2 || !val) {
		printf("Communication failure.\n");
		goto cleanup;
	}

	for (bit = 1; val >= bit; bit *= 2) {
		switch (val & bit) {
			case 1:
				printf("Controller is ready.\n");
				break;
			case 2:
				printf("Zero position reached.\n");
				break;
			case 4:
				printf("Position error.\n");
				break;
			default:
				break;
		}
	}

cleanup:
	free(line);
}

void
stepper_step_mode()
{
	int val;
	size_t n = 0;
	char *str, *line = NULL;

	if (int_arg(&val))
		RETPRINT("Please specify the step mode\n");

	fdflush(serial_fd, STEPPER_RECV_TIMEOUT);
	dprintf(serial_fd, "#%ig%i\n", stepper_id, val);

	str = alloc_fmtstr("%ig%i\n", stepper_id, val);
	if (await_input(serial_fd, STEPPER_RECV_TIMEOUT) > 0
			&& sgetline(&line, &n, serial_stream) != -1
			&& line && !strcmp(line, str)) {
		printf("Value set.\n");
	} else {
		printf("Communication failure.\n");
	}

	free(line);
	free(str);
}

void
list_commands()
{
	int i;

	for (i = 0; i < ARRSIZE(commands); i++)
		printf("- %s\n", commands[i].name);
}

void
info_msg()
{
	int i;

	printf("[33;1m");
	for (i = 1; i < arg_count; i++)
		printf("%s ", args[i]);
	printf("[0m\n");
}

void
serial_msg()
{
	int i, len;
	char *buf, *response = NULL;
	size_t n = 0;

	if (arg_count <= 1)
		RETPRINT("Please specify a serial message");

	for (len = 0, i = 1; i < arg_count; i++)
		len += strlen(args[i]) + 1;

	buf = malloc(len);
	for (len = 0, i = 1; i < arg_count; i++) {
		strcpy(&buf[len], args[i]);
		len += strlen(args[i]);
		buf[len++] = (i == arg_count - 1) ? '\0' : ' ';
	}

	fdflush(serial_fd, 500000);
	printf("SEND: %s\n", buf);
	dprintf(serial_fd, "%s\n", buf);
	free(buf);

	await_input(serial_fd, 500000);
	len = sgetline(&response, &n, serial_stream);
	if (response && n && len > 0)
		if (!*response) /* skip the occasional leading nullbyte */
			printf("RECV (%i): %s\n", len - 1, &response[1]);
		else
			printf("RECV (%i): %s\n", len, response);
	else
		printf("Nothing received.\n");

	free(response);
}

void
mqtt_connection_lost(void *ctx, char *cause)
{
	die("[MQTT] Connection lost..\n");
}

int
mqtt_receive(void *ctx, char *name, int len, MQTTClient_message *msg)
{
	char *key;
	printf("[MQTT] Message arrived (%s): %s\n", name, (char*) msg->payload);

	if (!strpfcmp(MQTT_STEPPER_TOPIC, name) && current_device == STEPPER) {
		key = &name[sizeof(MQTT_STEPPER_TOPIC) - 1];
		if (*key != '/')
			goto cleanup;
		key++;

		args[1] = msg->payload;
		arg_count = 2;

		if (!strcmp(key, "STEP")) {
			stepper_step();
		} else if (!strcmp(key, "STEPMODE")) {
			stepper_step_mode();
		}
	} else if (!strpfcmp(MQTT_STROBE_TOPIC, name) && current_device == STROBE) {
		key = &name[sizeof(MQTT_STROBE_TOPIC) - 1];
		if (*key != '/')
			goto cleanup;
		key++;

		args[1] = msg->payload;
		arg_count = 2;

		if (!strcmp(key, "POWER")) {
			strobe_power();
		} else if (!strcmp(key, "INTENSITY")) {
			strobe_intensity();
		}
	}

cleanup:
	MQTTClient_freeMessage(&msg);
	MQTTClient_free(name);
	return 1;
}

/* not relevant for QoS 1 */
void
mqtt_receive_ack(void *ctx, MQTTClient_deliveryToken dt)
{
	printf("[MQTT] RECV-ACK received.\n");
}

int
main(int argc, const char** argv)
{
	int i, len;
	char c, *line, *line_copy, *last_arg;
	const char* device_path;

	/* setup mqtt callbacks */
	{
		MQTTClient client;
		MQTTClient_connectOptions options = MQTTClient_connectOptions_initializer;
		int rc;
		char *id;

		if (argc == 1) {
			id = alloc_fmtstr(MQTT_CLIENTID_PRE "%d", getpid());
		} else {
			id = alloc_fmtstr(MQTT_CLIENTID_PRE "%s", argv[1]);
		}

		MQTTClient_create(&client, MQTT_ADDRESS, id,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);

		options.keepAliveInterval = 20;
		options.cleansession = 1;

		MQTTClient_setCallbacks(client, NULL, mqtt_connection_lost,
				mqtt_receive, mqtt_receive_ack);

		if ((rc  = MQTTClient_connect(client, &options)) != MQTTCLIENT_SUCCESS) {
			die("[MQTT] Failed to connect.. (%d)\n", rc);
		}

		if (MQTTClient_subscribe(client, MQTT_STROBE_TOPIC "/#", MQTT_QOS)
				!= MQTTCLIENT_SUCCESS)
			die("Failed to subscribe to strobe topic %s\n", MQTT_STROBE_TOPIC);
		if (MQTTClient_subscribe(client, MQTT_STEPPER_TOPIC "/#", MQTT_QOS)
				!= MQTTCLIENT_SUCCESS)
			die("Failed to subscribe to stepper topic %s\n", MQTT_STEPPER_TOPIC);

		printf("[MQTT] Subscribed to all topics as client '%s' (QoS:%d)\n",
				id, MQTT_QOS);

		free(id);
	}

	signal(SIGINT, &sighandler);
	line_copy = NULL;
	while (1) {
		/* flush everything that was not read (writes are synced)
		 * and wait an extra 100 ms to receive data */
		if (serial_fd != -1)
			fdflush(serial_fd, 100000);

		line = readline("> ");
		if (!line)
			break;
		if (!*line)
			goto continue_free;
		add_history(line);

		/* split into args */
		line_copy = strdup(line);
		last_arg = line;
		len = strlen(line);
		for (arg_count = i = 0; i <= len && arg_count < 20; i++) {
			if (line[i] == ' ' || line[i] == '\0') {
				if (last_arg == &line[i]) continue;
				args[arg_count] = last_arg;
				arg_count++;
				last_arg = &line[i+1];
				line[i] = '\0';
			}
		}
		
		if (arg_count == 20) {
			printf("Too many arguments!\n");
			goto continue_free;
		}

		for (i = 0; i < ARRSIZE(commands); i++) {
			if (!strcmp(commands[i].name, args[0])) {
				if (commands[i].target == ANY || (commands[i].target & current_device)) {
					cmd_done = 0;
					commands[i].callback();
					cmd_done = 1;
				} else {
					printf("Command target does not match current serial device (if connected)\n");
				}
				goto continue_free;
			}
		}

		if (i == ARRSIZE(commands))
			printf("Command not found\n");

continue_free:
		free(line_copy);
		line_copy = NULL;
		free(line);
		line = NULL;
	}

	printf("\n");
}
